package com.video.vidme.views;

import com.video.vidme.adapters.ViewPagerAdapter;

/**
 * Created by troll on 3/7/2017.
 */

public interface MainActivityView {

    ViewPagerAdapter getViewPagerAdapter();

    void getConnectionErrorMessage();
}

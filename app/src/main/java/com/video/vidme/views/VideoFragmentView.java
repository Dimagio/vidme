package com.video.vidme.views;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.video.vidme.adapters.VideoAdapter;

/**
 * Created by troll on 3/3/2017.
 */

public interface VideoFragmentView {

    void stopRefreshLayout();

    void startRefreshLayout();

    View getRootView();

    void setHasOptionMenu(boolean state);

    void setRecycleViewAndSwipeRefreshLayout();

    void setLoginButton();

    void showErrorMessage(int stringId);

    String getUsername();

    String getPassword();

    VideoAdapter getVideoAdapter();

    LinearLayoutManager getLinearLayoutManager();

    String getFragmentType();
}

package com.video.vidme.views.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.video.vidme.R;
import com.video.vidme.adapters.ViewPagerAdapter;
import com.video.vidme.presenters.implementation.MainPresenterImpl;
import com.video.vidme.presenters.intefaces.MainActivityPresenter;
import com.video.vidme.utils.Constants;
import com.video.vidme.views.MainActivityView;
import com.video.vidme.views.fragments.VideoFragment;

import java.util.ArrayList;
import java.util.List;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MainActivity extends AppCompatActivity implements MaterialTabListener, MainActivityView {

    ViewPager mViewPager;
    MaterialTabHost mMaterialTabHost;
    MainActivityPresenter mainActivityPresenter;
    ViewPagerAdapter mViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        mainActivityPresenter = new MainPresenterImpl(this, sharedPreferences);

        mMaterialTabHost = (MaterialTabHost) this.findViewById(R.id.materialTabHost);
        mViewPager = (ViewPager) this.findViewById(R.id.pager);

        setAdapter();
        setTabs();
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        return mainActivityPresenter.onUserLoggedOn();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mainActivityPresenter.onLogoutClicked(item);
        return super.onOptionsItemSelected(item);

    }

    @Override
    public ViewPagerAdapter getViewPagerAdapter() {
        return mViewPagerAdapter;
    }

    public void getConnectionErrorMessage() {
        Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
    }

    private void setAdapter() {
        VideoFragment featuredVideoFragment = new VideoFragment();
        VideoFragment newVideoFragment = new VideoFragment();
        VideoFragment feedVideoFragment = new VideoFragment();

        featuredVideoFragment.setFragmentType(Constants.FEATURED_VIDEOS);
        newVideoFragment.setFragmentType(Constants.NEW_VIDEOS);
        feedVideoFragment.setFragmentType(Constants.FEED_VIDEOS);

        List<VideoFragment> fragmentList = new ArrayList<>();
        fragmentList.add(featuredVideoFragment);
        fragmentList.add(newVideoFragment);
        fragmentList.add(feedVideoFragment);

        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        mViewPager.setAdapter(mViewPagerAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                mMaterialTabHost.setSelectedNavigationItem(position);
            }
        });
    }

    private void setTabs() {
        // insert all tabs from pagerAdapter data
        mMaterialTabHost.addTab(
                mMaterialTabHost.newTab()
                        .setText(getString(R.string.featured_title))
                        .setTabListener(this)
        );

        mMaterialTabHost.addTab(
                mMaterialTabHost.newTab()
                        .setText(getString(R.string.new_title))
                        .setTabListener(this)
        );

        mMaterialTabHost.addTab(
                mMaterialTabHost.newTab()
                        .setText(getString(R.string.feed_title))
                        .setTabListener(this)
        );
    }
}

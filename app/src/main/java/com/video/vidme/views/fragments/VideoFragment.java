package com.video.vidme.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.video.vidme.R;
import com.video.vidme.adapters.VideoAdapter;
import com.video.vidme.presenters.intefaces.VideoFragmentPresenter;
import com.video.vidme.presenters.implementation.VideoPresenterImpl;
import com.video.vidme.views.VideoFragmentView;

/**
 * Created by troll on 3/1/2017.
 */

public class VideoFragment extends Fragment implements VideoFragmentView {

    private String fragmentType;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private View rootView;
    private VideoAdapter videoAdapter;
    private VideoFragmentPresenter videoPresenter;
    private LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.video_fragment, container, false);

        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        videoPresenter = new VideoPresenterImpl(this, sharedPreferences);
        videoPresenter.createFragment(fragmentType);
        setHasOptionMenu(false);

        return rootView;

    }

    public void setFragmentType(String fragmentType) {
        this.fragmentType = fragmentType;
    }


    @Override
    public void stopRefreshLayout() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void startRefreshLayout() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public void setHasOptionMenu(boolean state) {
        setHasOptionsMenu(state);
    }

    @Override
    public void setRecycleViewAndSwipeRefreshLayout() {
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    videoPresenter.increaseVideoItems();
                }
            }
        });
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimaryDark));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                videoPresenter.onRefreshed();
            }
        });

        videoAdapter = new VideoAdapter();
        mLayoutManager = new LinearLayoutManager(getContext());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(videoAdapter);

        videoPresenter.loadVideoListToAdapter(true);
    }

    @Override
    public void setLoginButton() {
        Button mLoginButton = (Button) rootView.findViewById(R.id.button_log_in);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoPresenter.onLoginButtonClicked();
            }
        });
    }

    @Override
    public void showErrorMessage(int stringId) {
        Toast.makeText(getContext(), stringId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getUsername() {
        EditText username = (EditText) rootView.findViewById(R.id.username);
        return username.getText().toString();
    }

    @Override
    public String getPassword() {
        EditText username = (EditText) rootView.findViewById(R.id.password);
        return username.getText().toString();
    }

    @Override
    public VideoAdapter getVideoAdapter() {
        return videoAdapter;
    }

    @Override
    public LinearLayoutManager getLinearLayoutManager() {
        return mLayoutManager;
    }

    @Override
    public String getFragmentType() {
        return fragmentType;
    }
}

package com.video.vidme.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.video.vidme.views.fragments.VideoFragment;

import java.util.List;

/**
 * Created by troll on 3/1/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<VideoFragment> fragments;

    public ViewPagerAdapter(FragmentManager fm, List<VideoFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int pos) {
        return fragments.get(pos);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "VIDEO_FRAGMENT " + (position + 1);
    }
}

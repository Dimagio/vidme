package com.video.vidme.adapters;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.video.vidme.R;
import com.video.vidme.pojo.video.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by troll on 3/4/2017.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.Holder> {
    private List<Video> videos;

    public VideoAdapter() {
        this.videos = new ArrayList<>();
    }

    public void addVideos(List<Video> mVideos) {

        for (Video video : mVideos) {
            videos.add(video);
        }
        notifyDataSetChanged();
    }


    public void clearVideos() {
        videos.clear();
        notifyDataSetChanged();
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item_row, parent, false);

        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        Video currentVideo = videos.get(position);
        holder.setUrl(currentVideo.getCompleteUrl());
        if (currentVideo.getTitle() == null) {
            currentVideo.setTitle("Untitled");
        }
        holder.videoName.setText(currentVideo.getTitle());
        holder.videoLikes.setText(String.valueOf(currentVideo.getScore()));
        holder.thumbnailImage.getLayoutParams().height = currentVideo.getHeight();

        Picasso.with(holder.itemView.getContext()).load(currentVideo.getThumbnailUrl()).into(holder.thumbnailImage);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        String url;

        TextView videoName, videoLikes;
        ImageView thumbnailImage;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            videoName = (TextView) itemView.findViewById(R.id.video_name);
            videoLikes = (TextView) itemView.findViewById(R.id.video_likes);
            thumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(url), "video/mp4");
            v.getContext().startActivity(intent);
        }

        void setUrl(String url) {
            this.url = url;
        }
    }
}

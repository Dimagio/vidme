package com.video.vidme.pojo.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by troll on 3/7/2017.
 */

public class LogoutResponse {
    @SerializedName("status")
    private boolean status;

    public boolean isStatus() {
        return status;
    }
}

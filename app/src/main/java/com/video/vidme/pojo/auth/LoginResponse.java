package com.video.vidme.pojo.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by troll on 2/28/2017.
 */

public class LoginResponse {

    @SerializedName("auth")
    private Auth auth;

    public Auth getAuth() {

        return auth;

    }

    public static class Auth {

        @SerializedName("token")
        private String token;

        public String getToken() {
            return token;
        }

    }
}

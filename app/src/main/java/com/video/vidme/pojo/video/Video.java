package com.video.vidme.pojo.video;

import com.google.gson.annotations.SerializedName;

/**
 * Created by troll on 2/28/2017.
 */

public class Video {

    @SerializedName("complete_url")
    private String completeUrl;
    @SerializedName("title")
    private String title;
    @SerializedName("height")
    private Integer height;
    @SerializedName("thumbnail_url")
    private String thumbnailUrl;
    @SerializedName("score")
    private Integer score;

    public String getCompleteUrl() {
        return completeUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;

    }

    public Integer getHeight() {
        return height;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public Integer getScore() {
        return score;
    }
}

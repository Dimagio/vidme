package com.video.vidme.pojo.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by troll on 3/8/2017.
 */

public class LoginError {

    @SerializedName("error")
    private String error;

    public String getError() {
        return error;
    }
}

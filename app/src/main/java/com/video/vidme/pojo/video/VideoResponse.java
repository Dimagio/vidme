package com.video.vidme.pojo.video;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by troll on 2/28/2017.
 */

public class VideoResponse {
    @SerializedName("videos")
    private List<Video> videosList;

    public List<Video> getVideos() {
        return videosList;
    }
}

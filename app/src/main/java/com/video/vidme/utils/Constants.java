package com.video.vidme.utils;

/**
 * Created by troll on 2/28/2017.
 */

public class Constants {
    public final static String BASE_URL = "https://api.vid.me";
    public final static int NUMBER_OF_ITEMS = 10;

    public final static String NEW_VIDEOS = "new";
    public final static String FEATURED_VIDEOS = "featured";
    public final static String FEED_VIDEOS = "feed";

    public final static String SHARED_PREFERENCES_USERNAME = "username";
    public final static String SHARED_PREFERENCES_PASSWORD = "password";
    public final static String SHARED_PREFERENCES_ACCESS_TOKEN = "access_token";
    public final static String DEFAULT_USERNAME = "";
    public final static String DEFAULT_PASSWORD = "";
    public final static String DEFAULT_ACCESS_TOKEN = "0";
}

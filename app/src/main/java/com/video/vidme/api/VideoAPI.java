package com.video.vidme.api;

import com.video.vidme.pojo.video.VideoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by troll on 2/28/2017.
 */

public interface VideoAPI {

    @GET("/videos/{videoType}")
    Call<VideoResponse> setVideo(@Path("videoType") String videoType,
                                 @Query("offset") Integer numberOfStartVideo,
                                 @Query("limit") Integer numberOfItems);

    @GET("/videos/following")
    Call<VideoResponse> setFollowingVideo(@Query("token") String token,
                                          @Query("limit") Integer numberOfItems);
}

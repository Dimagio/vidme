package com.video.vidme.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.video.vidme.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by troll on 2/28/2017.
 */

public class RestApiManager {

    private Retrofit retrofit;

    private VideoAPI videoAPI;
    private AuthAPI authAPI;

    public RestApiManager() {
        Gson gson = new GsonBuilder().create();
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constants.BASE_URL)
                .build();
    }

    public VideoAPI getVideoAPI() {
        if (videoAPI == null) {
            videoAPI = retrofit.create(VideoAPI.class);
        }
        return videoAPI;
    }

    public AuthAPI getAuthAPI() {
        if (authAPI == null) {
            authAPI = retrofit.create(AuthAPI.class);
        }
        return authAPI;
    }

}

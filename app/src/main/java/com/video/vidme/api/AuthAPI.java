package com.video.vidme.api;

import com.video.vidme.pojo.auth.LoginResponse;
import com.video.vidme.pojo.auth.LogoutResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by troll on 3/5/2017.
 */

public interface AuthAPI {

    @FormUrlEncoded
    @POST("/auth/create")
    Call<LoginResponse> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("/auth/delete")
    Call<LogoutResponse> logout(@Field("token") String token);
}

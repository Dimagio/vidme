package com.video.vidme.presenters.implementation;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.video.vidme.R;
import com.video.vidme.api.RestApiManager;
import com.video.vidme.pojo.auth.LoginError;
import com.video.vidme.pojo.auth.LoginResponse;
import com.video.vidme.pojo.video.Video;
import com.video.vidme.pojo.video.VideoResponse;
import com.video.vidme.presenters.intefaces.VideoFragmentPresenter;
import com.video.vidme.utils.Constants;
import com.video.vidme.views.VideoFragmentView;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by troll on 3/3/2017.
 */

public class VideoPresenterImpl implements VideoFragmentPresenter {

    private VideoFragmentView videoFragmentView;
    private SharedPreferences sharedPreferences;

    private int offset = 0;
    private int limit = Constants.NUMBER_OF_ITEMS;


    public VideoPresenterImpl(VideoFragmentView videoFragmentView, SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        this.videoFragmentView = videoFragmentView;
    }


    /**
     * Check a type of fragment. In case if fragment belongs to featured or new videos, we show layout with
     * video items. If fragment belongs to feed videos, we should show a login layout if user is logged out,
     * and show layout with video items, if user logged in.
     * */
    @Override
    public void createFragment(String layoutType) {
        String username = sharedPreferences.getString(Constants.SHARED_PREFERENCES_USERNAME, Constants.DEFAULT_USERNAME);
        String password = sharedPreferences.getString(Constants.SHARED_PREFERENCES_PASSWORD, Constants.DEFAULT_PASSWORD);

        if (layoutType.equals(Constants.FEATURED_VIDEOS) || layoutType.equals(Constants.NEW_VIDEOS)) {
            videoFragmentView.setRecycleViewAndSwipeRefreshLayout();
        } else if (username.equals(Constants.DEFAULT_USERNAME) || password.equals(Constants.DEFAULT_PASSWORD)) {
            LinearLayout videoFragmentLayout = (LinearLayout) videoFragmentView
                    .getRootView()
                    .findViewById(R.id.video_fragment);
            videoFragmentLayout.setVisibility(View.GONE);

            ScrollView loginLayout = (ScrollView) videoFragmentView
                    .getRootView()
                    .findViewById(R.id.login_layout);
            loginLayout.setVisibility(View.VISIBLE);
            videoFragmentView.setLoginButton();
        } else {
            videoFragmentView.setRecycleViewAndSwipeRefreshLayout();
        }
    }

    /**
     * Load a videos to adapter. It`s standard algorithm for featured and new videos: we load items for
     * infinite scrolling using offset and standard limit (10 items) for request. Then add them to adapter.
     * For feed video we use just limit, increasing it by 10 for each request and add 10 last items to adapter.
     * Unfortunately, there is no offset for feed videos in vid.me api.
     * */
    @Override
    public void loadVideoListToAdapter(final boolean shouldStartRefreshLayout) {
        Call<VideoResponse> videoResponseCall;
        if (videoFragmentView.getFragmentType().equals(Constants.FEED_VIDEOS)) {
            String accessToken = sharedPreferences.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN,
                    Constants.DEFAULT_ACCESS_TOKEN);
            videoResponseCall = new RestApiManager()
                    .getVideoAPI()
                    .setFollowingVideo(accessToken, limit);
        } else
            videoResponseCall = new RestApiManager()
                    .getVideoAPI()
                    .setVideo(videoFragmentView.getFragmentType(), offset, limit);
        if (shouldStartRefreshLayout)
            videoFragmentView.startRefreshLayout();

        videoResponseCall.enqueue(new Callback<VideoResponse>() {
            @Override
            public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {
                VideoResponse videoResponse = response.body();
                List<Video> videos = videoResponse.getVideos();
                if (videoFragmentView.getFragmentType().equals(Constants.FEED_VIDEOS)
                        && limit > Constants.NUMBER_OF_ITEMS) {
                    //feed videos algorithm
                    int newFeedVideoStartPosition = videos.size() - Constants.NUMBER_OF_ITEMS;
                    videos = videoResponse.getVideos().subList(newFeedVideoStartPosition, videos.size());
                }
                videoFragmentView.getVideoAdapter().addVideos(videos);
                if (shouldStartRefreshLayout)
                    videoFragmentView.stopRefreshLayout();
            }

            @Override
            public void onFailure(Call<VideoResponse> call, Throwable t) {
                videoFragmentView.showErrorMessage(R.string.connection_lost);
                if (shouldStartRefreshLayout)
                    videoFragmentView.stopRefreshLayout();
            }
        });
    }

    /**
     * Algorithm for infinite scrolling. Read a bit more above of loadVideoListToAdapter method.
     * */
    @Override
    public void increaseVideoItems() {
        int lastVideoItem = videoFragmentView.getLinearLayoutManager().findLastVisibleItemPosition();
        if (videoFragmentView.getFragmentType().equals(Constants.FEED_VIDEOS)) {
            if (lastVideoItem == limit - 1) {
                limit += Constants.NUMBER_OF_ITEMS;
                loadVideoListToAdapter(false);
            }
        } else if (lastVideoItem == Constants.NUMBER_OF_ITEMS - 1 || lastVideoItem == offset) {
            offset += Constants.NUMBER_OF_ITEMS;
            loadVideoListToAdapter(false);
        }
    }

    /**
     * Logging method. It write a data to SharedPreferences for remembering user and show us layout
     * of feed videos and hide login layout. Also, show us errors in Toast that can appear, during the
     * authentication.
     * */
    @Override
    public void onLoginButtonClicked() {
        Call<LoginResponse> loginResponseCall = new RestApiManager()
                .getAuthAPI()
                .login(videoFragmentView.getUsername(), videoFragmentView.getPassword());
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Constants.SHARED_PREFERENCES_USERNAME, videoFragmentView.getUsername());
                    editor.putString(Constants.SHARED_PREFERENCES_PASSWORD, videoFragmentView.getPassword());
                    editor.putString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN, loginResponse.getAuth().getToken());
                    editor.apply();

                    ScrollView loginLayout = (ScrollView) videoFragmentView.getRootView().findViewById(R.id.login_layout);
                    loginLayout.setVisibility(View.GONE);

                    LinearLayout videoFragmentLayout = (LinearLayout) videoFragmentView
                            .getRootView()
                            .findViewById(R.id.video_fragment);
                    videoFragmentLayout.setVisibility(View.VISIBLE);
                    videoFragmentView.setHasOptionMenu(true);

                    videoFragmentView.setRecycleViewAndSwipeRefreshLayout();
                } else {
                    Gson gson = new GsonBuilder().create();
                    try {
                        LoginError loginError = gson.fromJson(response.errorBody().string(), LoginError.class);
                        Toast.makeText(videoFragmentView.getRootView().getContext(),
                                loginError.getError(),
                                Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                videoFragmentView.showErrorMessage(R.string.connection_lost);
            }
        });
    }

    /**
     * Load video items from start position. Refresh it.
     * */
    @Override
    public void onRefreshed() {
        videoFragmentView.getVideoAdapter().clearVideos();
        offset = 0;
        limit = Constants.NUMBER_OF_ITEMS;
        loadVideoListToAdapter(true);
    }

}

package com.video.vidme.presenters.implementation;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.video.vidme.R;
import com.video.vidme.api.RestApiManager;
import com.video.vidme.pojo.auth.LogoutResponse;
import com.video.vidme.presenters.intefaces.MainActivityPresenter;
import com.video.vidme.utils.Constants;
import com.video.vidme.views.MainActivityView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by troll on 3/7/2017.
 */

public class MainPresenterImpl implements MainActivityPresenter {

    private MainActivityView mainActivityView;
    private SharedPreferences sharedPreferences;
    private String token;

    public MainPresenterImpl(MainActivityView mainActivityView, SharedPreferences sharedPreferences) {
        this.mainActivityView = mainActivityView;
        this.sharedPreferences = sharedPreferences;
    }

    /**
     * Check if user logged on and show/not show us a "LOGOUT" menu
     * */
    @Override
    public boolean onUserLoggedOn() {
        String username = sharedPreferences.getString(Constants.SHARED_PREFERENCES_USERNAME, Constants.DEFAULT_USERNAME);
        String password = sharedPreferences.getString(Constants.SHARED_PREFERENCES_PASSWORD, Constants.DEFAULT_PASSWORD);
        token = sharedPreferences.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN, Constants.DEFAULT_ACCESS_TOKEN);

        return !(username.equals(Constants.DEFAULT_USERNAME) && password.equals(Constants.DEFAULT_PASSWORD));
    }


    /**
     * Clear user data from SharedPreferences and show us a login layout.
     * */
    @Override
    public void onLogoutClicked(final MenuItem item) {

        Call<LogoutResponse> logoutResponseCall = new RestApiManager().getAuthAPI().logout(token);
        logoutResponseCall.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                if (response.body().isStatus()) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(Constants.SHARED_PREFERENCES_USERNAME, Constants.DEFAULT_USERNAME);
                    editor.putString(Constants.SHARED_PREFERENCES_PASSWORD, Constants.DEFAULT_PASSWORD);
                    editor.putString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN, Constants.DEFAULT_ACCESS_TOKEN);

                    editor.apply();

                    int pos = mainActivityView.getViewPagerAdapter().getCount() - 1;
                    Fragment feedVideoFragment = mainActivityView.getViewPagerAdapter().getItem(pos);
                    if (feedVideoFragment != null) {
                        View fragmentView = feedVideoFragment.getView();
                        if (fragmentView != null) {
                            LinearLayout videoFragmentLayout = (LinearLayout) fragmentView.findViewById(R.id.video_fragment);
                            videoFragmentLayout.setVisibility(View.GONE);

                            ScrollView loginLayout = (ScrollView) fragmentView.findViewById(R.id.login_layout);
                            loginLayout.setVisibility(View.VISIBLE);
                            feedVideoFragment.setHasOptionsMenu(false);
                            EditText usernameEditText = (EditText) fragmentView.findViewById(R.id.username);
                            EditText passwordEditText = (EditText) fragmentView.findViewById(R.id.password);

                            usernameEditText.setText("");
                            passwordEditText.setText("");
                        }
                    }
                    item.setVisible(false);
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                mainActivityView.getConnectionErrorMessage();
            }
        });

    }
}

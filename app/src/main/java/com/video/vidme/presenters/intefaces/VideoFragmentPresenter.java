package com.video.vidme.presenters.intefaces;

/**
 * Created by troll on 3/3/2017.
 */

public interface VideoFragmentPresenter {

    void createFragment(String resourceId);

    void loadVideoListToAdapter(boolean shouldSwipeRefreshLayout);

    void increaseVideoItems();

    void onLoginButtonClicked();

    void onRefreshed();
}

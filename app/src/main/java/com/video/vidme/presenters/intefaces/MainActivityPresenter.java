package com.video.vidme.presenters.intefaces;

import android.view.MenuItem;

/**
 * Created by troll on 3/7/2017.
 */

public interface MainActivityPresenter {

    boolean onUserLoggedOn();

    void onLogoutClicked(MenuItem item);
}
